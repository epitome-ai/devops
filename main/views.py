from rest_framework.views import APIView
from rest_framework.response import Response


class HealthView(APIView):
	def get(self, request):
		if request.method == 'GET':
			res = {
			'statusCode': 200,
			'message': 'OK'
			}
		return Response(res)


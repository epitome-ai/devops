# DevOps Assignment
---
This assignment will be a test the candidate on different parts of the DevOps cycle. We will evaluate your skills in the following areas:

1. Understanding of the problem
2. Docker knowledge
3. Azure knowledge
4. Kubernetes knowledge
5. CI/CD knowledge
6. Monitoring/alerting

You have 24 hours to submit the assignment. The idea is not to finish every task on the assignment but finish as much as you can from each section to finish a successful deployment. 


## Requirements
---
In order to complete the assignment, you will need an Azure Subscription. You can speak to your point of contact regarding this. 


## Problem Statement
---
### Code set up
To complete the assignment you need a working Django application. You can use the one in this repository. The Dockerfile provided will run the application on port 8000. Download the code and upload it into a private repository on github owned by you. 

### Environment set up
Tasks:

1. set up YAML files for Kubernetes deployment configs
2. setup Azure deployments using azure-cli
3. setup an autoscaling Kubernetes cluster on Azure

You will also need to set up a PostgreSQL database on Azure. You can change the following lines in the ```app/settings.py``` to the credentials you need for the app to function. 
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
}
```
to as follows:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'foo',
        'USER': 'foo@bar',
        'PASSWORD': 'baz',
        'HOST': 'qux.postgres.database.azure.com',
        'PORT': '5432',
        },
}
```
where the relevant values are changed as required. 

Questions:

1. How could you automate the creation of all environment resources to allow to easily create new environments for development and testing?
2. What could you parametrize to give the environment creation automation more flexibility?

### Continuous Integration
Create a build pipeline (ideally in Jenkins) that builds the code from the repository you created with the fork.

Tasks

1. set up YAML files for whichever CI/CD tool you chose to use.
2. Trigger the builds automatically after each commit on master.
3. Run unit tests for every build.

Questions:

1. What kind of reporting would you generate for this pipeline?
2. What kind of notifications would you generate?
3. What is a good strategy to trigger automatic builds for feature, release and hotfix branches, in your opinion?

### Continuous Delivery
Create a release pipeline that allows you to deploy the builds created in the previous point to the Azure.

Tasks

1. Create the release automatically after a build in master is successfully finished.
2. Deploy the release automatically after the master build.

Questions:

1. If the company has 4 teams, describe a proposed environment and release pipeline that allows them to test and release to production the software they produce. Note that we don't specify if each team works on a single service or they work on the whole solution, which might impact your proposed set up. 
Feel free to define a workflow that you thing works correctly.
2. What kind of reporting would you generate for this pipeline?
3. What kind of notifications would you generate?

### Production Operations
Tasks:

1. Trigger an alert whenever CPU exceeds a %. 
2. Configure autoscaling so that when the average CPU exceeds a % the cluster increases its capacity.

Questions:

1. What kind of monitoring would you create for a system like this?
2. What kind of alerts would you create?
3. What kind of dashboard would you create?
4. What metrics and strategies would you use to control the system capacity/scaling?

### Documentation
Prepare a README.md file and add it to the repository.

Tasks:

1. Explain your solutions and decisions (use the questions in the above sections as a template if you need)
2. List the tools and technologies you used.
3. Explain thehe problems and challenges that you have faced.
4. List the further improvements possible. 


## Submission
---
Submit the github repository that you used for the assignment. 
It should include all the relevant YAML files, scripts for handling infrastructure, scripts for setting up environments, all the other utilities you might have used for the completion of the assignment and documentation for thr assignment. 
